import { Component, OnInit, Input } from '@angular/core';
import { CardsService } from '../../services/cards.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() cardPos: number;
  activeCardPos: number;
  distanceFromActiveCard: number;


  constructor(private _cardsService: CardsService) { }

  ngOnInit() {
    this._cardsService.getActiveCardPos().subscribe(resActiveCardPos => {
      this.activeCardPos = resActiveCardPos;
      this.distanceFromActiveCard = (this.activeCardPos > this.cardPos) ? (this.activeCardPos - this.cardPos) : (this.cardPos - this.activeCardPos); 
    });
  }

  onCardClick(): void {
    this._cardsService.setActiveCardPos(this.cardPos);
  }

}
