import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from '../../../node_modules/rxjs';

@Injectable()
export class CardsService {

  activeCardPos$: Subject<number> = new BehaviorSubject<number>(2); 

  constructor() { }

  getActiveCardPos(): Observable<number> {
    return this.activeCardPos$.asObservable();
  }

  setActiveCardPos(hours: number): void {
    this.activeCardPos$.next(hours);
  }

}
